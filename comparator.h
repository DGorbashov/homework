#ifndef COMPARATOR_H
#define COMPARATOR_H
#include <QString>
#include <QStringList>
#include "folder.h"

class Comparator
{
public:
    Comparator();

    bool        filesIsEqual(QString firstPath, QString secondPath);
    QStringList comporation(Folder& firstFolder, Folder& secondFolder);
};

#endif // COMPARATOR_H
