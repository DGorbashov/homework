#include "comparator.h"
#include <QCryptographicHash>
#include <QFile>
#include <algorithm>
#include <QTextStream>
#include <QDebug>
#include "folder.h"

Comparator::Comparator()
{

}

bool Comparator::filesIsEqual(QString firstPath, QString secondPath)
{
    QFile firstFile(firstPath);
    QFile secondFile(secondPath);

    if(!firstFile.open(QIODevice::ReadOnly)){
        qDebug() << "Can't read " << firstFile.fileName();
         return 0;
    }
    if(!secondFile.open(QIODevice::ReadOnly)){
        qDebug() << "Can't read " << secondFile.fileName();
        return 0;
    }

    QTextStream firstFileStream(&firstFile);
    QTextStream secondFileStream(&secondFile);
    QString firstFileRow;
    QString secondFileRow;

    do
    {
        firstFileRow = firstFileStream.readLine();
        secondFileRow = secondFileStream.readLine();
    }
    while(firstFileRow == secondFileRow && !firstFileStream.atEnd());

    if(firstFileRow != secondFileRow){
        qDebug() << "false";
        return false;
    }

    qDebug() << "true";
    return true;

}



QStringList Comparator::comporation(Folder &firstFolder, Folder &secondFolder)
{
    QStringList equalFiles;

    for(int i = 0; i < firstFolder.size(); i++) {
        QFile tmp(firstFolder.getPath() + firstFolder.getFile(i));


        qDebug() << tmp.fileName() << " size: " << tmp.size();
        std::vector<qint64>::iterator matchPos = std::find(secondFolder.getFoldersFirstFileSize(),
                                                           secondFolder.getFoldersLastFileSize(),
                                                           tmp.size());

        int matchPosition = std::distance(secondFolder.getFoldersFirstFileSize(),
                                          matchPos);

        if (matchPosition != secondFolder.size()){
          for(int j = matchPosition; secondFolder.getSize(matchPos) == tmp.size(); j++){

              qDebug() << j << secondFolder.getFile(j) << " size: " << secondFolder.getSize(matchPos);
               bool compare = filesIsEqual(QString(firstFolder.getPath() + firstFolder.getFile(i)),
                                           QString(secondFolder.getPath() + secondFolder.getFile(j)));
               if (compare){
                   QString tmp =  firstFolder.getFile(i) + " " + secondFolder.getFile(j);
                   equalFiles << tmp;
               }
               ++matchPos;
          }
        }
    }

    if (equalFiles.isEmpty())
        equalFiles << "No matches";

    return equalFiles;
}
