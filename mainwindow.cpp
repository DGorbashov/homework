#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QDir>
#include <QMessageBox>
#include <QStringListModel>
#include <QCryptographicHash>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_buttonForFirstDir_clicked()
{
    QString newDir = QFileDialog::getExistingDirectory(this, tr("Open Directory"), QDir::homePath(), QFileDialog::ShowDirsOnly);
    if(newDir.isEmpty()){
        QMessageBox::warning(this, "Warning", "Choose the directory");
        return;
    }
    newDir = newDir + '/';
    if(newDir == firstFolder.getPath()){
        QMessageBox::warning(this, "Warning", "You try to choose the same directory\nPlease, choose anothe directory");
        return;
    }
    if(newDir == secondFolder.getPath()){
        QMessageBox::warning(this, "Warning", "You try to compare equal directories\nPlease, choose another directory");
        return;
    }
    if(!firstFolder.isEmpty()){
        ui->firstFolderListView->clear();
    }

    if(!equalFiles.isEmpty()){
        ui->equalFilesListView->clear();
    }

    firstFolder.update(newDir);
    updateFirstDirLabel(firstFolder.getPath());

    for(int i = 0; i < firstFolder.size(); i++)
    {
        ui->firstFolderListView->append(firstFolder.getFile(i));
    }
}

void MainWindow::on_buttonForSecondDir_clicked()
{
    QString newDir = QFileDialog::getExistingDirectory(this, tr("Open Directory"), QDir::homePath(), QFileDialog::ShowDirsOnly) + '/';
    if(newDir.isEmpty()){
        QMessageBox::warning(this, "Warning", "Choose the directory");
        return;
    }
    if(newDir == secondFolder.getPath()){
        QMessageBox::warning(this, "Warning", "You try to choose the same directory\nPlease, choose anothe directory");
        return;
    }
    if(newDir == firstFolder.getPath()){
        QMessageBox::warning(this, "Warning", "You try to compare equal directories\nPlease, choose another directory");
        return;
    }
    if(!secondFolder.isEmpty()){
        ui->secondFolderListView->clear();
    }
    if(!equalFiles.isEmpty()){
        ui->equalFilesListView->clear();
    }

    secondFolder.update(newDir);
    updateSecondDirLabel(secondFolder.getPath());

    for(int i = 0; i < secondFolder.size(); i++)
    {
        ui->secondFolderListView->append(secondFolder.getFile(i));
    }

}

void MainWindow::updateFirstDirLabel(QString path)
{
    ui->firstDirLabel->setText(path);
}

void MainWindow::updateSecondDirLabel(QString path)
{
    ui->secondDirLabel->setText(path);
}

void MainWindow::on_buttonForComparing_clicked()
{
    if(firstFolder.isEmpty() || secondFolder.isEmpty()){
        QMessageBox::warning(this, "Warning", "Choose directories");
        return;
    }

    if(!equalFiles.isEmpty()){
        ui->equalFilesListView->clear();
        equalFiles.clear();
    }

    equalFiles = comparator.comporation(firstFolder, secondFolder);

    for(int i=0 ; i < equalFiles.length() ; i++){
       ui->equalFilesListView->append(equalFiles.at(i));
    }
}
