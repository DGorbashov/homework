#ifndef FOLDER_H
#define FOLDER_H

#include <QString>
#include <QStringList>

class Folder
{
public:
    Folder();
    QString getPath();
    QString getFile(int i);
    qint64  getSize(std::vector<qint64>::iterator i);
    bool    isEmpty();
    void    update (QString newPath);
    int     size();

    std::vector<qint64>::iterator getFoldersFirstFileSize();
    std::vector<qint64>::iterator getFoldersLastFileSize();

private:
    QString             path;
    QStringList         foldersFiles;
    std::vector<qint64> foldersFilesSizes;

    void    setPath(QString newPath);
};

#endif // FOLDER_H
