#include "folder.h"
#include <QFileDialog>
#include <QMessageBox>

Folder::Folder()
{

}

void Folder::setPath(QString newPath)
{
    path = newPath;
}

QString Folder::getPath()
{
    return path;
}

void Folder::update(QString newPath)
{
        setPath(newPath);

        if(!foldersFiles.isEmpty()){
            foldersFiles.clear();
            foldersFilesSizes.clear();
        }

        QDir myDir(newPath);
        foldersFiles = myDir.entryList(QDir::Files, QDir::Size);

        for(int i=0 ; i < foldersFiles.length() ; i++){
            QFile tmp(path + foldersFiles.at(i));
            foldersFilesSizes.push_back(tmp.size());
        }
}

QString Folder::getFile(int i)
{
    if (foldersFiles.isEmpty()){
          return "";
    }
    if (i > foldersFiles.size()){
        return "";
    }
    return foldersFiles.at(i);
}

qint64 Folder::getSize(std::vector<qint64>::iterator i)
{
    if (foldersFilesSizes.empty()){
        return -1;
    }
    if (i > foldersFilesSizes.end()){
        return -1;
    }
    return *i;
}

bool Folder::isEmpty()
{
    return foldersFiles.isEmpty();
}

int Folder::size()
{
    return foldersFiles.size();
}

std::vector<qint64>::iterator Folder::getFoldersFirstFileSize()
{
    return foldersFilesSizes.begin();
}

std::vector<qint64>::iterator Folder::getFoldersLastFileSize()
{
    return foldersFilesSizes.end();
}
