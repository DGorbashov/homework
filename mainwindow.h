#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStringList>
#include "folder.h"
#include "comparator.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_buttonForFirstDir_clicked();
    void updateFirstDirLabel(QString path);
    void updateSecondDirLabel(QString path);
    void on_buttonForSecondDir_clicked();
    void on_buttonForComparing_clicked();

private:
    Ui::MainWindow *ui;

    Folder firstFolder;
    Folder secondFolder;

    Comparator comparator;

    QStringList equalFiles;
};

#endif // MAINWINDOW_H
